#!/bin/bash

pushd $(dirname $0) > /dev/null

pcount=$(ps -f|grep tau.sh|egrep -vc grep)
if [ $pcount -eq 0 ]; then
  ./tau.sh &
  sleep 1
fi

pid=$(ps -f|grep tau.sh|egrep -v grep | awk '{print $2}')
kill -s SIGUSR1 $pid

popd > /dev/null
