#!/usr/bin/python
import signal
import sys
import time

tau=""
def signal_handler(signal, frame):
  print(tau)

signal.signal(signal.SIGUSR1, signal_handler)

with open('tau.dat') as f:
  for line in f:
    tau=line.rstrip('\n')
    signal.pause()

