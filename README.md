# Pre-Requisites: #
1. Bash
2. Python2.7+

# Installation: #
1. Clone this repository

# Usage: #
1. Call <signal>/read.sh to get a value

# Signals: #
* **tau** - Temperature of KGPZ weather station between 2015-12-01 and 2016-02-29, 15-minute resolution